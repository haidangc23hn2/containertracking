const adjacentModel = require('../models/adjacentList');
const {euclidDistance} = require('../utils/utils');

const addEdge = async (vertex1, vertex2) => {
  await addDirectEdge(vertex1, vertex2);
  await addDirectEdge(vertex2, vertex1);
}

const addDirectEdge = (vertex1, vertex2) => {
  let distance = euclidDistance(vertex1.coordinate, vertex2.coordinate);
  adjacentModel
    .findOneAndUpdate(
      {
        _id: vertex1._id  // search query
      }, 
      {
        $push: {adjacentVertex: {_id: vertex2._id, weight: distance}}   // field:values to update
      },
      {
        new: true,                       // return updated doc
        runValidators: true,              // validate before update
        upsert: true, 
        setDefaultsOnInsert: true
      })
    .then(doc => {
      //console.log(doc)
    })
    .catch(console.error);
}

module.exports = addEdge;
const portModel = require('../models/port');

// const findPortID = (startPort, endPort) => {
//   return new Promise((resolve, reject) =>
//     portModel.find({$or: [
//       {'name': { $regex: new RegExp("^" + startPort.toLowerCase(), "i") }}, 
//       {'name': { $regex: new RegExp("^" + endPort.toLowerCase(), "i") }}
//     ]})
//       .exec((err, docs) => {
//         if(err) reject(err);
        
//         if(docs.length === 2){
//           if(docs[0].name.toLowerCase() === docs[1].name.toLowerCase()){
//             resolve([docs[0].coordinateID, docs[1].coordinateID]);
//           }
//           else{
//             resolve([docs[1].coordinateID, docs[0].coordinateID]);
//           }
//         }
//         else{
//           reject('Can not found all port');
//         }
//     })
//   )
// }

const findPortID = (portName) => {
  return new Promise((resolve, reject) =>
    portModel.find({$or: [
      {'name': { $regex: new RegExp("^" + portName.toLowerCase(), "i") }}
    ]})
      .exec((err, docs) => {
        if(err) reject(err);
        
        docs.length == 1 && resolve(docs[0].coordinateID);
        docs.length == 0 && reject('Port ID not found!');
    })
  )
}

module.exports = {
  findPortID: findPortID
};
const Graph = require('../utils/graph');
const vertexModel = require('../models/vertex');
const adjacentListModel = require('../models/adjacentList');

const buildGraph = async () => {
  graph = new Graph();
  await addNodeToGraph(graph);
  await addEdgeToGraph(graph);
  return graph;
}

const addNodeToGraph = graph => {
  return new Promise((resolve, reject) =>
    vertexModel.find({})
    .sort({'_id': 1})
    .select('_id coordinate')
    .exec((err, docs) => {
      if (err) throw reject(err);
  
      docs.forEach(vertex => {  
        graph.addNode({
          id: vertex._id,
          coordinate: vertex.coordinate
        });
      })
      resolve()
    })
  )
}

const addEdgeToGraph = graph => {
  return new Promise((resolve, reject) =>
    adjacentListModel.find({})
    .sort({'_id': 1})
    .exec((err, docs) => {
      if(err) throw reject(err);

      docs.forEach(srcVertex => {
        srcVertex.adjacentVertex.forEach(desVertex => {
          graph.addDirectedEdge(srcVertex._id, desVertex._id, desVertex.weight);
        })
      })

      resolve();
    })
  )
};

module.exports = {
  buildGraph: buildGraph
}
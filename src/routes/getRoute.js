const express = require('express');
const router = express.Router();
const {findComplexRoute} = require('../functions/findComplexRouteFunction');

router.get('/', (req, res) => {
  const {listPortName, lastVisitedPortName, shipCoordinate} = req.query;
  findComplexRoute(res, listPortName.split(','), lastVisitedPortName, shipCoordinate.split(','));
})

module.exports = router;
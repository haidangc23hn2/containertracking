const express = require('express');
const vertexModel = require('../models/vertex');
const addEdge = require('../functions/addEdgeFunction');
const router = express.Router();

router.post('/', (req, res) => {
  const {type} = req.body;
  vertexModel
    .find({type: type})
    .select('_id coordinate')
    .sort({"_id": 1})
    .exec((err, docs) => {
      if(err) throw err;

      for(let i = 0; i < docs.length - 1; i++){
        addEdge(docs[i], docs[i+1]);
      }
    }) 
  res.json('DONE');
})

module.exports = router;
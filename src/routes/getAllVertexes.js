const express = require('express');
const vertexModel = require('../models/vertex'); 

const router = express.Router();

router.get('/', (req, res) => {
  vertexModel.find({}).select('_id coordinate').exec((err, docs) => {
    if(err) throw err;

    let listV = [];
    docs.forEach(doc => {
      listV.push(doc);
    })

    res.json(listV);
  })
})

module.exports = router;
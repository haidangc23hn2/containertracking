const express = require('express');
const getAllEdgesCoordinate = require('../functions/getAllEdgesCoordinateFunction');
const getAllEdgesID = require('../functions/getAllEdgesIDFunction');

const router = express.Router();

router.get('/', async (req, res) => {
  let edgesListByID = [];
  let edgesListByCoordinate = []
  await getAllEdgesID(edgesListByID);
  await getAllEdgesCoordinate(edgesListByID,edgesListByCoordinate);
  res.json({edgesList: edgesListByCoordinate});
})

module.exports = router;
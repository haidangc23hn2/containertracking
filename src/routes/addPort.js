const express = require('express');
const addPort = require('../functions/addPortFunction');
const router = express.Router();

router.post('/', (req, res) => {
  const {port} = req.body;
  if(typeof port !== 'object'){
    addPort(JSON.parse(port));
  }
  else{
    addPort(port);
  }
  res.json('Add port has been done!!!');
})

module.exports = router;
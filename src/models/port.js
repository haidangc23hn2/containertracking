let {Schema, model} = require('mongoose');

let portSchema = new Schema({
  _id: Number,
  name: String,
  codeID: String,
  coordinateID: Number
  }, {collection: 'Port'}
)

module.exports = model('Port', portSchema);
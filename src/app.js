const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const db = require('./database');
//const buildGraph = require('./routes/buildGraph');
const getAllVertexes = require('./routes/getAllVertexes');
const getAllEdges = require('./routes/getAllEdges.js');
const addVertex = require('./routes/addVertex');
const addEdge = require('./routes/addEdge');
const addEdgeByID = require('./routes/addEdgeByID');
const addPort = require('./routes/addPort');
const getRoute = require('./routes/getRoute');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());
//app.use('/', buildGraph);
app.use('/getAllVertexes', getAllVertexes);
app.use('/getAllEdges', getAllEdges);
app.use('/addVertex', addVertex);
app.use('/addEdge', addEdge);
app.use('/addEdgeByID', addEdgeByID);
app.use('/addPort', addPort);
app.use('/getRoute', getRoute);

app.listen(PORT, () => {
  console.log(`App listen on PORT ${PORT}`);
})